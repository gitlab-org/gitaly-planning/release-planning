require 'httparty'
require 'active_support/core_ext/string'

class IssueGenerator
  # group_name is just passed from arguments so this class has to transform it
  #   project_management
  def initialize(group_name: nil, template_filename: nil)
    @group_name = group_name
    @template_filename = template_filename || group_template_filename
  end

  def to_json
    {
      title: title,
      description: description_text.gsub(/\[milestone\]/, upcoming_milestone['title'])
    }
  end

  def title
    @title ||= begin
                 if @template_filename.include?('Monthly')
                   msdate = Date.parse(upcoming_milestone['due_date']).strftime("(%B %Y)")
                   "#{human_group_name} #{upcoming_milestone['title']} Planning #{msdate}"
                 else 
                   @template_filename.gsub(/-/, ' ').gsub(/\.md/, '').camelize + " #{Date.today.strftime('%Y-%m-%d')}"
                 end
               end
  end

  def description_text
    file = File.expand_path("../../.gitlab/issue_templates/#{@template_filename}", __FILE__)

    unless File.exist?(file)
      raise RuntimeError.new("Expected a file called #{@template_filename} to exist, but it doesn't. Try creating it and running again.")
    end

    File.read(file)
  end

  private

  def upcoming_milestone
    @upcoming_milestone ||= begin
                              current_milestone = release_milestone_for(Date.today)
                              return nil unless current_milestone
                              date = Date.parse(current_milestone['due_date']) + 1
                              release_milestone_for(date)
                            end
  end

  def release_milestone_for(date)
    milestones.detect do |m|
      next unless m['start_date'] && m['due_date']

      (Date.parse(m['start_date'])..Date.parse(m['due_date'])).include?(date) &&
        m['title'] =~ /\A\d{1,2}\.\d{1,2}\z/
    end
  end

  def milestones
    @milestones ||= begin
                      url = "https://gitlab.com/api/v4/groups/9970/milestones?state=active&per_page=100"
                      headers = { 'PRIVATE-TOKEN' => ENV['PLANNING_PROJECT_TOKEN'] }
                      HTTParty.get(url, headers: headers).parsed_response
                    end

  end

  def human_group_name
    @group_name.to_s.split('_').map(&:capitalize).join(' ')
  end

  def group_template_filename
    'Monthly-' + @group_name.split('_').map(&:capitalize).join('-') + '.md'
  end
end
