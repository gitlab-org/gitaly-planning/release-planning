> _This issue and linked pages contain information related to upcoming products, features, and functionality.  It is important to note that the information presented is for informational purposes only. Please do not rely on this information for purchasing or planning purposes.  As with all projects, the items mentioned in this video and linked pages are subject to change or delay. The development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc._


**Table of Contents**

[[_TOC_]]

## Boards

<details><summary>Issue boards we use for planning</summary>

- [Gitaly Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/3999272?label_name[]=group%3A%3Agitaly)
- [Gitaly Engineering Board](https://gitlab.com/groups/gitlab-org/-/boards/1140874?milestone_title=Upcoming&label_name[]=group%3A%3Agitaly)

</details>

* [Full list of release dates](https://about.gitlab.com/releases/)
* Gitaly [Objectives](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=created_date&state=opened&label_name%5B%5D=group%3A%3Agitaly&type%5B%5D=objective&first_page_size=20) and [Key Results](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=created_date&state=opened&label_name%5B%5D=group%3A%3Agitaly&type%5B%5D=key_result&first_page_size=20) - GitLab Internal Only

## Capacity Notes

## Objectives and Themes

Please see our direction page section [What's Next & Why](https://about.gitlab.com/direction/gitaly/) for our ongoing focus. The list below are specific pieces from these themes.

## Planning Inputs

_Note: The Gitaly team currently does not have a UX component for planning inputs._

### Engineering Priorities

### Quality Priorities

[Bug Prioritization Sisense Board](https://app.periscopedata.com/app/gitlab/1037965/Bug-Prioritization) - Internal Only. Filter on `team_group = gitaly` to see Gitaly specific.


## References

<details><summary>Handbook pages which are useful</summary>

- [Gitaly Direction Page](https://about.gitlab.com/direction/gitaly/)
- [Gitaly Engineering Page](https://about.gitlab.com/handbook/engineering/development/enablement/systems/gitaly/)

</details>

[Gitaly Kickoff Video]()

/cc @eread

/assign @mjwood @andrashorvath @jcaigitlab

/label ~"group::gitaly" ~"Planning Issue" ~"Category:Gitaly" ~"section::core platform"  ~"devops::systems" ~type::maintenance ~maintenance::release
