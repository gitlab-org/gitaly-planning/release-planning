> _This issue and linked pages contain information related to upcoming products, features, and functionality.  It is important to note that the information presented is for informational purposes only. Please do not rely on this information for purchasing or planning purposes.  As with all projects, the items mentioned in this video and linked pages are subject to change or delay. The development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc._

*Please note: GitLab does not own the Git project, but we are proud supporters. Therefore, the efforts planned here are contributions we intend on making to the next Git release pending their approval and acceptance by the Git developer community.*

**Table of Contents**

[[_TOC_]]

## Checklist
- [ ] Update the issue title to `Gitaly Git Contribution Planning - Git Version x.xx`
- [ ] Update the link to the latest Git Planned Contribution Board below to have the correct milestone

## Boards

<details><summary>Issue boards we use for planning</summary>

- [Git Planned Contribution Board](https://gitlab.com/groups/gitlab-org/-/boards/7706409?milestone_title=Git%202.47&label_name[]=group%3A%3Agitaly%3A%3Agit)

</details>

## Capacity Notes

## Objectives and Themes

As a general guideline, the following breakdown should be followed when
scheduling issues for a Git milestone.

| Type of issue | Examples | Percentage of team capacity  |
| ------------- | -------- | ---------------------------  |
| ~"type::feature" | [dynamically enable mailmap in cat-file --batch](https://gitlab.com/gitlab-org/git/-/issues/344) | 55% |
| ~"type::bug"| [pack unreachable loose objects](https://gitlab.com/gitlab-org/git/-/issues/336) | 20% |
| ~"type::maintenance"| [fix memory leak in clone](https://gitlab.com/gitlab-org/git/-/issues/358) | 10% |

The other 15% of the teams' capacity should be left open for issues that the Git
maintainer, Junio might request. Junio acts like a second PM for our team as he
will from time to time request certain fixes or refactors. Doing these behooves
our team by increasing visibility and standing in the Git community.

Please see our direction page section [Git](https://about.gitlab.com/direction/gitaly/#git) for our ongoing focus. The list below are specific pieces from these themes.

## Planning Inputs

_Note: The Gitaly team currently does not have a UX component for planning inputs._

### Engineering Priorities

## References

<details><summary>Handbook pages which are useful</summary>

- [Gitaly Git Direction Page](https://about.gitlab.com/direction/gitaly/#git)
- [Gitaly Engineering Page](https://about.gitlab.com/handbook/engineering/development/enablement/systems/gitaly/)

</details>

[Git Kickoff Video]()

/assign @mjwood @andrashorvath @jcaigitlab

/label ~"group::gitaly" ~"group::gitaly::git"  ~"Planning Issue" ~"Category:Gitaly" ~"section::core platform"  ~"devops::systems" ~type::maintenance ~maintenance::release
